import { useState, useReducer } from "react";
import { range } from "lodash";

type Action = number | "+" | "-" | "x" | "/" | "reset";

type StatePart = {
  type: string;
  value: Action;
};

export default () => {
  const [history, dispatchToHistory]: [
    StatePart[],
    (value: Action) => void
  ] = useReducer((prevState: StatePart[], action: Action) => {
    // The action is a number
    if (typeof action === "number") {
      if (prevState.length === 0) {
        return [
          {
            type: "number",
            value: action
          }
        ];
      }
      if (prevState[prevState.length - 1].type === "number") {
        const lastArrayElement = prevState[prevState.length - 1];

        const newState = [...prevState];
        newState[newState.length - 1] = {
          type: "number",
          value: +(lastArrayElement.value.toString() + action.toString())
        };
        return newState;
      }
      return [
        ...prevState,
        {
          type: "number",
          value: action
        }
      ];
    }
    // The action is an operator
    if (action === "x" || action === "+" || action === "-" || action === "/") {
      if (prevState.length === 0) {
        return [
          {
            type: "operator",
            value: action
          }
        ];
      }
      if (prevState[prevState.length - 1].type === "operator") {
        const newState = [...prevState];
        newState[newState.length - 1] = {
          type: "operator",
          value: action
        };
        return newState;
      }
      return [
        ...prevState,
        {
          type: "operator",
          value: action
        }
      ];
    }
    if (action === 'reset') {
      if (prevState.length === 0) {
        return [
          {
            type: "reset",
            value: action
          }
        ];
      }
      if (prevState[prevState.length - 1].type === "reset") {
        const newState = [...prevState];
        newState[newState.length - 1] = {
          type: "reset",
          value: action
        };
        return newState;
      }
      return [
        ...prevState,
        {
          type: "reset",
          value: action
        }
      ];
    }
  }, []);

  return (
    <>
      <div>
        {(["+", "-", "/", "x"] as Action[]).map(operator => (
          <button
            key={operator}
            onClick={() => {
              dispatchToHistory(operator);
            }}
          >
            {operator}
          </button>
        ))}
        <button
          onClick={() => {
            dispatchToHistory("reset");
          }}
        >
          reset
        </button>
      </div>
      <div>
        {range(10).map(number => {
          return (
            <button
              key={number}
              onClick={() => {
                dispatchToHistory(number);
              }}
            >
              {number}
            </button>
          );
        })}
      </div>
      <div>
        outcome:{" "}
        {(() => {
          let result: number = 0;
          for (let index = 0; index < history.length; index++) {
            const element = history[index];
            if (element.type === "operator") {
              continue;
            }
            if ((!history[index - 1] || history[index - 1].type === 'reset') && element.type === "number") {
              // @ts-ignore
              result += element.value;
              continue;
            }
            if (element.type === "reset") {
              result = 0;
              continue;
            }
            if (history[index - 1] && history[index - 1].type === "operator") {
              switch (history[index - 1].value) {
                case "+":
                  // @ts-ignore
                  result += element.value;
                  break;
                case "-":
                  // @ts-ignore
                  result -= element.value;
                  break;
                case "/":
                  // @ts-ignore
                  result /= element.value;
                  break;
                case "x":
                  // @ts-ignore
                  result *= element.value;
                  break;

                default:
                  break;
              }
              continue;
            }
          }
          return result;
        })()}
      </div>
      <div>
        {history.map(historyItem => {
          return <div>{historyItem.value} </div>;
        })}
      </div>
    </>
  );
};
