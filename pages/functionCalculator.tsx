import { useState } from "react"

export default () => {
  const [state, setState] = useState('');

  return (
    <>
    <input value={state} onChange={e => {
      setState(e.target.value)
    }}></input>
    <div>
      output:{' '}
      {(() => {
        try {
          const result = Function('"use strict";return (' + state + ')')()
          if(typeof result === 'number') {
            return result
          }
          throw new Error('no')
        }
        catch(e) {
          return 'Error: Malformed Input'
        }
      })()}
    </div>
    </>
  )
}